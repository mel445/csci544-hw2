def splitter(infilename, filename1, filename2):
	from random import shuffle

	with open(infilename, 'r') as f:
        	lines = f.readlines()

	shuffle(lines)

	with open(filename1, 'w') as f:
		f.writelines(lines[:int(len(lines)*3 // 4)])
    	with open(filename2, 'w') as f:
        	f.writelines(lines[int(3*len(lines) // 4):])


def main():

	infilename="enron_train.txt"
	filename1="test2.txt"
	filename2="trainenron1.txt"
	splitter(infilename,filename1,filename2)

if __name__ == '__main__':
	main()
